package file;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PortalUtil;
import file.constants.FilePortletKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + FilePortletKeys.FILE,
                "mvc.command.name=handleForm"
        },
        service = MVCActionCommand.class

)

public class TestClass implements MVCActionCommand {

    private static final Log _log = LogFactoryUtil.getLog(TestClass.class);

    @Override
    public boolean processAction(
            ActionRequest actionRequest, ActionResponse actionResponse)
            throws PortletException {
        UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
        File uploadedFile = uploadPortletRequest.getFile("fileUpload");
        String fileName = uploadedFile.getName();
        File saveFile = new File("C:\\liferay-workspace\\NodeJs\\" + fileName);
        try {
            saveFile.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        FileWriter myWriter;
        try {
            Scanner myReader = new Scanner(uploadedFile);
            String data = null;
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
            }
            myReader.close();
            myWriter = new FileWriter(saveFile);
            myWriter.write(data);
            myWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }



        String url = "http://localhost:7777/loadinternalfile?filename=" + fileName;

        try {
            URL urlObj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                String fileContent = response.toString();
                actionRequest.setAttribute("myText", fileContent);
                System.out.println(fileContent);
            } else {
                _log.error("GET request to " + url + " failed with HTTP error code " + responseCode);
            }
        } catch (Exception e) {
            _log.error("Error while calling HTTP GET request to " + url + ": " + e.getMessage());
        }
        saveFile.delete();
        return true;
    }
}


