<%@ include file="/init.jsp" %>
<portlet:actionURL name="handleForm" var="actionURL"/>
<aui:form action="<%= actionURL %>" style="margin-top: 2rem;" enctype="multipart/form-data" method="post">
	<aui:input type="file" id="fileUpload" name="fileUpload"/>
	<aui:button-row>
		<aui:button type="submit" value="send"/>
	</aui:button-row>
</aui:form>
