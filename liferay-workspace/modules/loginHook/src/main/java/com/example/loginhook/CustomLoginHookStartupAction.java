package com.example.loginhook;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;
public class CustomLoginHookStartupAction extends SimpleAction {

    @Override
    public void run(String[] lifecycleEventIds) throws ActionException {
        for (String eventId : lifecycleEventIds) {
            System.out.println("Startup event ID " + eventId);
        }
    }

}