package com.example.loginhook;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author blegt
 */
public class CustomLoginHookLoginPreAction extends Action {

	@Override
	public void run(HttpServletRequest request, HttpServletResponse response) {
		//long userId = PortalUtil.getUserId(request);

		//User user = UserLocalServiceUtil.fetchUser(userId);

		System.out.println("Pre action");
	}

}

/*
package com.example.loginhook;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;

import org.osgi.service.component.annotations.Component;

@Component(
		immediate = true,
		property = {
				"key = login.events.pre"
		},
		service = LifecycleAction.class
)
public class CustomLoginHookLoginPreAction implements LifecycleAction {

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {
		System.out.println("After login");
	}
}*/