package com.example.loginhook;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.events.ActionException;


import static com.liferay.portal.kernel.util.PortalUtil.*;

/**
 * @author blegt
 */
public class CustomLoginHookLoginPostAction extends Action {

    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException  {

        long userId = getUserId(request);

        User user = UserLocalServiceUtil.fetchUser(userId);

        System.out.println(user.getFirstName() + " has logged in.");
        /*
        try {
            response.sendRedirect("/file");
        } catch (Exception e) {
            throw new ActionException(e);
        }*/
    }

}
/*
package com.example.loginhook;

        import com.liferay.portal.kernel.events.ActionException;
        import com.liferay.portal.kernel.events.LifecycleAction;
        import com.liferay.portal.kernel.events.LifecycleEvent;

        import org.osgi.service.component.annotations.Component;

@Component(
        immediate = true,
        property = {
                "key = login.events.post"
        },
        service = LifecycleAction.class
)
public class CustomLoginHookLoginPostAction implements LifecycleAction {

    @Override
    public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {
        System.out.println("Before login");
    }
}*/