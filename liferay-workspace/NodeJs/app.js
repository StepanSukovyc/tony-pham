const express = require('express');
const app = express();
const fs = require('fs');
const port = 7777;

const OpenApiValidator = require('express-openapi-validator');

app.use(
  OpenApiValidator.middleware({
    apiSpec: './openapi.yaml',
    validateRequests: true,
    validateResponses: true,
  }),
);

// Definice GET metody pro načtení souboru
app.get('/loadinternalfile', (req, res) => {
  const filename = req.query.filename; // získání názvu souboru z query parametru
  const filepath = `./${filename}`; // sestavení cesty k souboru
  
  // Čtení souboru pomocí funkce readFile z knihovny fs
  fs.readFile(filepath, 'utf8', (err, data) => {
    if (err) {
      res.status(404).send('Soubor nebyl nalezen'); // Pokud soubor není nalezen, vrátíme chybu 404
    } else {
      res.send(data); // Pokud soubor byl nalezen, vrátíme jeho obsah
    }
  });
});

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Server běží na portu ${port}`);
});